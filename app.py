#!/usr/bin/env python3

import aws_cdk as cdk

from lambdanewsletter.lambdanewsletter_stack import LambdanewsletterStack


app = cdk.App()
LambdanewsletterStack(app, "lambdanewsletter")

app.synth()
